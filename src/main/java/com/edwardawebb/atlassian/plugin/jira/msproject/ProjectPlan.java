/**
 * Copyright Oct 14, 2012 Edward A. Webb
 * 
 */
package com.edwardawebb.atlassian.plugin.jira.msproject;

import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.lang.Validate;
import org.slf4j.LoggerFactory;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.issue.link.LinkCollection;
import com.atlassian.jira.issue.priority.Priority;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.plugin.searchrequestview.SearchRequestViewModuleDescriptor;
import com.edwardawebb.atlassian.plugin.jira.msproject.ProjectMeta.ProjectMetaBuilder;
import com.edwardawebb.atlassian.plugin.jira.msproject.utilities.ProjectUIDIncrementer;
import com.edwardawebb.atlassian.plugin.jira.msproject.utilities.ProjectUtils;

/**
 * @author Edward A. Webb
 */
public class ProjectPlan {
	private static org.slf4j.Logger LOG = LoggerFactory
			.getLogger(ProjectPlan.class);

	private ProjectMeta projectMeta;

	/**
	 * Long issueId, ProjectTask task
	 */
	Map<Long, ProjectTask> tasks;
	// list of assignees that gets appended after tasks
	/**
	 * String username,ProjectResource resource
	 */
	Map<String, ProjectResource> resources;
	// list of mappings to get appended after taks ansd assignees
	Set<ProjectAssignment> assignments;

	private ProjectPlan(ProjectPlanBuilder builder) {
		Validate.notNull(builder.assignments);
		Validate.notNull(builder.resources);
		Validate.notNull(builder.projectMetaBuilder);
		Validate.notNull(builder.tasks);
		this.assignments = builder.assignments;
		this.tasks = builder.tasks;
		this.projectMeta = builder.projectMetaBuilder.build();
		this.resources = builder.resources;
	}

	public void write(Writer writer,
			SearchRequestViewModuleDescriptor descriptor,
			Map<String, Object> availableParams) throws SearchException,
			IOException {

		writeHeader(writer, descriptor, availableParams);

		writeTasks(writer, descriptor, availableParams);

		writeResources(writer, descriptor, availableParams);

		writeAssignments(writer, descriptor, availableParams);

		writeFooter(writer, descriptor, availableParams);
	}

	private void writeHeader(Writer writer,
			SearchRequestViewModuleDescriptor descriptor,
			Map<String, Object> availableParams) throws IOException {
		writer.write(descriptor.getHtml("header"));
		projectMeta.write(writer, descriptor, availableParams);

	}

	private void writeTasks(Writer writer,
			SearchRequestViewModuleDescriptor descriptor,
			Map<String, Object> availableParams) throws SearchException,
			IOException {
		Map<String, Object> myParams = new HashMap<String, Object>(
				availableParams);

		writer.write(descriptor.getHtml("tasks-header"));
		for (ProjectTask task : tasks.values()) {
			myParams.put("task", task);
			writer.write(descriptor.getHtml("tasks-each", myParams));
		}
		writer.write(descriptor.getHtml("tasks-footer"));
	}

	private void writeResources(Writer writer,
			SearchRequestViewModuleDescriptor descriptor,
			Map<String, Object> availableParams) throws SearchException,
			IOException {
		Map<String, Object> myParams = new HashMap<String, Object>(
				availableParams);
		writer.write(descriptor.getHtml("resources-header"));
		myParams.put("resource", this);
		for (ProjectResource resource : resources.values()) {
			myParams.put("resource", resource);
			writer.write(descriptor.getHtml("resources-each", myParams));
		}
		writer.write(descriptor.getHtml("resources-footer"));
	}

	private void writeAssignments(Writer writer,
			SearchRequestViewModuleDescriptor descriptor,
			Map<String, Object> availableParams) throws SearchException,
			IOException {
		Map<String, Object> myParams = new HashMap<String, Object>(
				availableParams);
		myParams.put("assignment", this);
		writer.write(descriptor.getHtml("assignments-header"));
		for (ProjectAssignment assignment : assignments) {
			myParams.put("assignment", assignment);
			writer.write(descriptor.getHtml("assignments-each", myParams));
		}
		writer.write(descriptor.getHtml("assignments-footer"));
	}

	private void writeFooter(Writer writer,
			SearchRequestViewModuleDescriptor descriptor,
			Map<String, Object> availableParams) throws IOException {
		writer.write(descriptor.getHtml("footer"));

	}

	/**
	 * Creates a ProjectPlan after calling {@link #withIssue(Issue)} for each
	 * issue encountered in search
	 * 
	 * @author Edward A. Webb
	 */
	public static class ProjectPlanBuilder {

		public static TreeSet<Long> BLOCKING_LINK_TYPE_IDS = new TreeSet<Long>();

		private final ProjectUIDIncrementer uidIncrementer = new ProjectUIDIncrementer();
		private ProjectMetaBuilder projectMetaBuilder = new ProjectMetaBuilder();

		private Map<Long, ProjectTask> tasks = new TreeMap<Long, ProjectTask>();
		// list of assignees that gets appended after tasks
		private Map<String, ProjectResource> resources = new TreeMap<String, ProjectResource>();
		// list of mappings to get appended after taks ansd assignees
		private Set<ProjectAssignment> assignments = new HashSet<ProjectAssignment>();
		private IssueLinkManager issueLinkManager;

		public ProjectPlanBuilder(IssueLinkManager issueLinkManager) {
			this.issueLinkManager = issueLinkManager;
			BLOCKING_LINK_TYPE_IDS.add(10000L);
		}

		/**
		 * Add the issue and create necessary Resource and Assignments as
		 * needed. Will also update {@link ProjectMeta} with earliest start and
		 * latest finish
		 * 
		 * @param issue
		 */
		public void withIssue(Issue issue) {

			ProjectTask task = new ProjectTask(issue, uidIncrementer.getUID());
			tasks.put(task.getIssue().getId(), task);
			
			createAssignment(task, uidIncrementer.getUID());

			projectMetaBuilder.updateStartAndFinishIfExceeded(task);
		}

		private void createAssignment(ProjectTask task,
				 String uid) {
			if(null == task.getIssue().getAssignee()){return;}
			ProjectResource resource = updateOrCreateResource(task);
			ProjectAssignment assignment = new ProjectAssignment(task,
					resource, uid);
			assignments.add(assignment);
		}

		private ProjectResource updateOrCreateResource(ProjectTask task) {			
			String resourceKey = task.getIssue().getAssignee().getName();
			if (resources.containsKey(resourceKey)) {
				return updateResourceAllocation(task, resourceKey);
			} else {
				return createResourceAllocation(task, resourceKey);
			}
		}

		private ProjectResource createResourceAllocation(ProjectTask task,
				String resourceKey) {
			User assignee = task.getIssue().getAssignee();
			ProjectResource resource = new ProjectResource(
					uidIncrementer.getUID());
			resource.setId(assignee.getName());
			resource.setName(assignee.getDisplayName());
			resource.setInitials(ProjectUtils.buildInitials(assignee
					.getDisplayName()));
			resource.ammendWork(task);
			resources.put(resourceKey, resource);
			return resource;
		}

		private ProjectResource updateResourceAllocation(ProjectTask task,
				String resourceKey) {
			ProjectResource resource = resources.get(resourceKey);
			resource.ammendWork(task);
			return resource;
		}


		/**
		 * Completes any processing required on all the issues included via
		 * {@link #withIssue(Issue)} to return a full {@link ProjectPlan}, ready
		 * for storage or printing via
		 * {@link ProjectPlan#write(Writer, SearchRequestViewModuleDescriptor, Map)}
		 * 
		 * @return
		 */
		public ProjectPlan build() {

			calculatePredecessors();

			return new ProjectPlan(this);
		}

		private void calculatePredecessors() {
			if (issueLinkManager.isLinkingEnabled())
				for (ProjectTask task : tasks.values()) {
					Issue issue = task.getIssue();
					List<IssueLink> links = issueLinkManager
							.getInwardLinks(issue.getId());
					for (IssueLink issueLink : links) {
						Issue linkedIssue = issueLink.getSourceObject();
						LOG.debug(
								"ISSUE {} HAS INWARD LINK, SOURCE IS {}",
								issue.getKey(), linkedIssue.getKey());
						LOG.debug("ISSUE LINK TYPE ID {}",issueLink.getLinkTypeId());
						if (BLOCKING_LINK_TYPE_IDS.contains(issueLink.getLinkTypeId())
								&& tasks.containsKey(Long.valueOf(Long.valueOf(linkedIssue.getId())))) {
							LOG.debug("This issue has FINISH TO START predecessor that is present in export");
							task.addPredecessor(tasks.get(linkedIssue.getId()));
						}
					}
				}
		}

	}

}
