/**
 * Copyright Oct 14, 2012 Edward A. Webb
 * 
 */
package com.edwardawebb.atlassian.plugin.jira.searchrequest;

import java.io.IOException;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.lucene.search.IndexSearcher;
import org.joda.time.Period;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchProviderFactory;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.views.util.IssueWriterHitCollector;
import com.atlassian.jira.issue.worklog.Worklog;
import com.atlassian.jira.issue.worklog.WorklogManager;
import com.atlassian.jira.plugin.searchrequestview.AbstractSearchRequestView;
import com.atlassian.jira.plugin.searchrequestview.RequestHeaders;
import com.atlassian.jira.plugin.searchrequestview.SearchRequestParams;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.JiraDurationUtils;
import com.atlassian.jira.util.JiraDurationUtils.PrettyDurationFormatter;
import com.atlassian.jira.util.JiraVelocityUtils;

/**
 * @author Edward A. Webb
 */
public class WorklogRssView extends AbstractSearchRequestView {
	private static Logger LOG = LoggerFactory.getLogger(WorklogRssView.class);
	
	private final JiraAuthenticationContext authenticationContext;
	private final SearchProviderFactory searchProviderFactory;
	private final IssueFactory issueFactory;
	private final SearchProvider searchProvider;
	private final WorklogManager worklogManager;
	private final SimpleDateFormat rfceighttwotwo = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z");
	private List<Worklog> allWorkLogs;
	PrettyDurationFormatter formatter ;
	private final String baseUrl = ComponentAccessor.getApplicationProperties().getString(APKeys.JIRA_BASEURL);
	
	public WorklogRssView(
			JiraAuthenticationContext authenticationContext,
			SearchProviderFactory searchProviderFactory,
			IssueFactory issueFactory, SearchProvider searchProvider,
			WorklogManager worklogManager) {
		this.authenticationContext = authenticationContext;
		this.searchProviderFactory = searchProviderFactory;
		this.issueFactory = issueFactory;
		this.searchProvider = searchProvider;
		this.worklogManager = worklogManager;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.atlassian.jira.plugin.searchrequestview.AbstractSearchRequestView
	 * #writeSearchResults(com.atlassian.jira.issue.search.SearchRequest,
	 * com.atlassian.jira.plugin.searchrequestview.SearchRequestParams,
	 * java.io.Writer)
	 */
	@Override
	public void writeSearchResults(SearchRequest searchRequest,
			SearchRequestParams searchRequestParams, Writer writer) throws SearchException {
		
		
		allWorkLogs = new ArrayList<Worklog>();
		
	
		final Map<String,Object> defaultParams = JiraVelocityUtils.getDefaultVelocityParams(authenticationContext);
		
		formatter = new JiraDurationUtils.PrettyDurationFormatter(8,5,authenticationContext.getI18nHelper());
		
		
        defaultParams.put("filtername", searchRequest.getName());
        defaultParams.put("user",authenticationContext.getLoggedInUser());
        defaultParams.put("baseUrl",baseUrl);
        
        try {
			writer.write(descriptor.getHtml("header",defaultParams));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
         
        try
        {
        	final IndexSearcher searcher = searchProviderFactory.getSearcher(SearchProviderFactory.ISSUE_INDEX);        
		       
        	//define what to do with every issue matched. 
	        final IssueWriterHitCollector hitCollector = new IssueWriterHitCollector(searcher, writer, issueFactory)
	        {
	            protected void writeIssue(Issue issue, Writer writer) throws IOException
	            {
	            	LOG.debug("Retreiving logs for {}",issue);
	            	List<Worklog> worklogs = worklogManager.getByIssue(issue);
	            	LOG.debug("Adding {} entries",worklogs.size());
	        		allWorkLogs.addAll(worklogs);	        		
	            }
	        };
	        
			//now run the search that's defined in the issue navigator and pass in the hitcollector from above which will
	        //collect the details for each issue, and build out our plan
	        searchProvider.searchAndSort(searchRequest.getQuery(), authenticationContext.getLoggedInUser(), hitCollector, searchRequestParams.getPagerFilter());

	        

	        if(allWorkLogs.size()>0){
		        //sort logs
		        Comparator<Worklog> c = new WorklogComparator();
		        Collections.sort(allWorkLogs, c);
		        
		        //print logs
		        Map<String, Object> myParams = new HashMap<String, Object>(
	    				defaultParams);

		        for (Worklog worklog : allWorkLogs) {
					myParams.put("worklog", worklog);
					myParams.put("time", formatter.format(worklog.getTimeSpent()));
					myParams.put("issuelink", baseUrl + "/browse/" + worklog.getIssue().getKey());
					myParams.put("workloglink", baseUrl + "/browse/" + worklog.getIssue().getKey() + "?focusedWorklogId=" + worklog.getId() + "&amp;page=com.atlassian.jira.plugin.system.issuetabpanels%3Aworklog-tabpanel#worklog-" + worklog.getId());
					//http://localhost:2990/jira/browse/DEMO-2?focusedWorklogId=10005&page=com.atlassian.jira.plugin.system.issuetabpanels%3Aworklog-tabpanel#worklog-10005
					myParams.put("published", rfceighttwotwo.format(worklog.getCreated()));
					writer.write(descriptor.getHtml("item",myParams));
				}
	        }
	        
	        
	        
	        writer.write(descriptor.getHtml("footer"));
			

	        
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
        catch (SearchException e)
        {
            throw new RuntimeException(e);
        }
        
        
        
        

	}

	
	  public Period toJiraPeriod(Period effort) {
		  LOG.debug(effort.toString());
          int hours = effort.normalizedStandard().getHours();
          int days = effort.getDays();
          if(hours <= 8 && days == 0){
                  return effort;
          }else{
        	  days = ((days*24)/8);
        	  days +=  (effort.getHours()/8 );
        	  int rem = effort.getHours() - (8*days);
        	  Period period = new Period().withDays(days).withHours(rem).withSeconds(effort.getSeconds());
              
               return period;
          }
  }


	@Override
	public void writeHeaders(SearchRequest searchRequest,
			RequestHeaders requestHeaders) {
		super.writeHeaders(searchRequest, requestHeaders);
		

		
		
	}
	
	
	
	

}
