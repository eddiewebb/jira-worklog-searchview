/**
 * Copyright Oct 14, 2012 Edward A. Webb
 * 
 */
package com.edwardawebb.atlassian.plugin.jira.msproject.utilities;

/**
 * MS Project uses an Integer based UID that is unique in the scope of a project.
 * SInce an export represnts an entire project we just start at 1 and count
 * See @{link http://msdn.microsoft.com/en-us/library/office/bb968590(v=office.12).aspx}
 * @author Edward A. Webb
 */
public class ProjectUIDIncrementer {
	private int quoteUnQuoteUid = 1;
	
	public String getUID(){
		return ""+quoteUnQuoteUid++;
	}

}
