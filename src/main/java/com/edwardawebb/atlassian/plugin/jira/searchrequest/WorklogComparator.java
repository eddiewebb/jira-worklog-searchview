/**
 * Copyright 2013 
 * jira-worklog-searchview - 
 *
 */
package com.edwardawebb.atlassian.plugin.jira.searchrequest;

import java.util.Comparator;
import com.atlassian.jira.issue.worklog.Worklog;
/**
 * @author Edward Webb
 *
 */
public class WorklogComparator implements Comparator<Worklog> {

	@Override
	public int compare(Worklog o1, Worklog o2) {
		return o2.getCreated().compareTo(o1.getCreated());
	}

}
