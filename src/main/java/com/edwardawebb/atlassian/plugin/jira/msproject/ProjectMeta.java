/**
 * Copyright Oct 22, 2012 Edward A. Webb
 * 
 */
package com.edwardawebb.atlassian.plugin.jira.msproject;

import java.io.IOException;
import java.io.Writer;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.Validate;

import com.atlassian.jira.plugin.searchrequestview.SearchRequestViewModuleDescriptor;

/**
 * Incorporates values that describe the {@link ProjectPlan} including start,
 * end, creation and formats
 * 
 * @author Edward A. Webb
 */
public class ProjectMeta {

	private static String TEMPLATE_HEADER_REFERENCE = "meta";

	private ProjectDateTime start;
	private ProjectDateTime finish;
	private ProjectDateTime creation;
	private int minutesPerDay = 480;

	private ProjectMeta(ProjectMetaBuilder builder) {
		Validate.notNull(builder.creation);
		Validate.notNull(builder.start);
		Validate.notNull(builder.finish);
		this.start = builder.start;
		this.creation = builder.creation;
		this.finish = builder.finish;
		this.minutesPerDay = builder.minutesPerDay;
	}

	/**
	 * Writes out metadata using configured templates
	 * 
	 * @param writer
	 * @param descriptor
	 * @param availableParams
	 * @throws IOException
	 */
	public void write(Writer writer,
			SearchRequestViewModuleDescriptor descriptor,
			Map<String, Object> availableParams) throws IOException {
		Map<String, Object> myParams = new HashMap<String, Object>(
				availableParams);
		myParams.put("meta", this);
		writer.write(descriptor.getHtml(TEMPLATE_HEADER_REFERENCE, myParams));
	}

	public ProjectDateTime getStart() {
		return start;
	}

	public ProjectDateTime getFinish() {
		return finish;
	}

	public ProjectDateTime getCreation() {
		return creation;
	}

	public int getMinutesPerDay() {
		return minutesPerDay;
	}

	public static class ProjectMetaBuilder {

		private ProjectDateTime start;
		private ProjectDateTime finish;
		private ProjectDateTime creation;
		private int minutesPerDay = 480;

		public ProjectMetaBuilder() {
			this.start = new ProjectDateTime(new Date());
			this.creation = new ProjectDateTime(new Date());
			this.finish = new ProjectDateTime(new Date());
		}

		public void withStart(ProjectDateTime start) {
			this.start = start;
		}

		public void withEnd(ProjectDateTime finish) {
			this.finish = finish;
		}

		public void withMinutesPerDay(int minutesPerDay) {
			this.minutesPerDay = minutesPerDay;
		}

		public ProjectMeta build() {
			return new ProjectMeta(this);
		}

		public void updateStartAndFinishIfExceeded(ProjectTask task) {
			if (task.getStart().isBefore(this.start)) {
				this.start = task.getStart();
			} else if (task.getFinish().isAfter(this.finish)) {
				{
					this.finish = task.getFinish();
				}
			}

		}

	}
}
