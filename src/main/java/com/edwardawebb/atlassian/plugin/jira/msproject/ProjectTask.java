/**
 * Copyright Oct 14, 2012 Edward A. Webb
 * 
 */
package com.edwardawebb.atlassian.plugin.jira.msproject;




import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.priority.Priority;
import com.atlassian.jira.project.version.Version;

/**
 * @author Edward A. Webb
 */
public class ProjectTask  {

	private static Logger LOG = LoggerFactory.getLogger(ProjectTask.class);
	
	private final Issue issue;
	private final String uid  ;
	private final String name;
	private Long priorityOrder;
	private ProjectDuration originalEffort;
	private ProjectDuration actualWork;
	private ProjectDuration remainingWork;
	private ProjectDateTime start;
	private ProjectDateTime finish;
	private ProjectDateTime earlyStart;
	private ProjectDateTime earlyFinish;
	private int percentComplete;
	private Set<String> predecessors = new TreeSet<String>();
	
	public ProjectTask(Issue issue, String uid) {		
		this.issue = issue;
		this.uid =uid;
		this.name = StringEscapeUtils.escapeJava(StringEscapeUtils.escapeXml(issue.getSummary()));

		Priority priority = issue.getPriorityObject();
		this.priorityOrder = priority.getSequence();
		
		populateDurationFields();
	}
	
	
	private void populateDurationFields() {
		LOG.debug("Determining efforts and dates for issue {}",issue.getKey());
		Long originalEstimate = issue.getOriginalEstimate();
		if(null == originalEstimate){originalEstimate = 0L;}
		if(null != issue.getEstimate() && issue.getEstimate().longValue() > originalEstimate ){
			originalEstimate = issue.getEstimate();
		}
		this.originalEffort = new ProjectDuration(originalEstimate*1000);
		if(null == issue.getTimeSpent()){
			this.actualWork = new ProjectDuration(0);
		}else{
			this.actualWork = new ProjectDuration(issue.getTimeSpent()*1000);
		}	
		
		calculateFinishDate();
		
		//should this  move for resolved tasks? i.e. iuse actual work? 
		//Seems the assignee would not know that when statting, and the orginal effortis more likly when they started.
		this.start = this.finish.toStartBasedOnEffort(originalEffort);
		
		LOG.debug("Result: {}",toStringVerbose());			
	}



	/**
	 * Works to find a finish date in this order:
	 * Resolution Date
	 * Due Date
	 * Earliest FixVersion Release Date
	 * Agile Sprint FInish Date
	 * Today
	 */
	private void calculateFinishDate() {
		//is this issue resolved? 
		this.finish = new ProjectDateTime(new Date());
				if(null == issue.getResolutionDate()){
					if(null == issue.getDueDate()){
						//last resort is this issue beloning to an active sprint..
						boolean isDated = false;
						//arbritrarily futuristic date
						ProjectDateTime earliestAssignedReleaseDate = new ProjectDateTime(finish.getDateTime().plusYears(10));
						for(Version version : issue.getFixVersions()){
							if(null != version.getReleaseDate() && earliestAssignedReleaseDate.isAfter(version.getReleaseDate())){
								earliestAssignedReleaseDate = new ProjectDateTime(version.getReleaseDate());
								isDated = true;
							}
						}
						if(isDated){
							this.finish = earliestAssignedReleaseDate;
						}else{
							//get date from sprint
						}
					}else{
						this.finish = new ProjectDateTime(issue.getDueDate(),true);
					};
					if(null == issue.getEstimate()){
						this.remainingWork = new ProjectDuration(0);//JIRA API lies and this returns seconds, not milliseconds
						this.percentComplete = 0;
					}else{
						this.remainingWork = new ProjectDuration(issue.getEstimate()*1000);//JIRA API lies and this returns seconds, not milliseconds
						this.percentComplete = 100 - (int)(((double)remainingWork.absoluteSeconds().getSeconds()/(double)originalEffort.absoluteSeconds().getSeconds())*100);
						if(this.percentComplete < 0){
							LOG.warn("Unexpected % complete less then 0");
							this.percentComplete = 0;
						}
						if(this.percentComplete > 100){
							LOG.warn("Unexpected % complete greater than 100");
							this.percentComplete = 100;
						}
					}
				}else{
					this.finish = new ProjectDateTime(issue.getResolutionDate(),true);
					this.remainingWork = new ProjectDuration(0);//JIRA API lies and this returns seconds, not milliseconds				
					this.percentComplete = 100 ;		
				}
	}


	public void addPredecessor(ProjectTask projectTask) {
		predecessors.add(projectTask.getUid());		
	}

	public Set<String> getPredecessors() {
		return predecessors;
	}


	public ProjectDuration getOriginalEffort() {
		return originalEffort;
	}


	public void setOriginalEffort(ProjectDuration duration) {
		this.originalEffort = duration;
	}


	public ProjectDuration getActualWork() {
		return actualWork;
	}


	public void setActualWork(ProjectDuration actualWork) {
		this.actualWork = actualWork;
	}


	public ProjectDuration getRemainingWork() {
		return remainingWork;
	}


	public void setRemainingWork(ProjectDuration remainingWork) {
		this.remainingWork = remainingWork;
	}


	public ProjectDateTime getStart() {
		return start;
	}


	public void setStart(ProjectDateTime start) {
		this.start = start;
	}


	public ProjectDateTime getFinish() {
		return finish;
	}


	public void setFinish(ProjectDateTime finish) {
		this.finish = finish;
	}


	public ProjectDateTime getEarlyStart() {
		return earlyStart;
	}


	public void setEarlyStart(ProjectDateTime earlyStart) {
		this.earlyStart = earlyStart;
	}


	public ProjectDateTime getEarlyFinish() {
		return earlyFinish;
	}


	public void setEarlyFinish(ProjectDateTime earlyFinish) {
		this.earlyFinish = earlyFinish;
	}


	public Issue getIssue() {
		return issue;
	}


	public String getUid() {
		return uid;
	}


	public int getPercentComplete() {
		return percentComplete;
	}


	public String getName() {
		return name;
	}

	public Long getPriorityOrder() {
		return priorityOrder;
	}


	public void setPriorityOrder(Long prioritySequence) {
		this.priorityOrder = prioritySequence;
	}


	private Object toStringVerbose() {
		return ReflectionToStringBuilder.toString(this);
	}






	
	
}
