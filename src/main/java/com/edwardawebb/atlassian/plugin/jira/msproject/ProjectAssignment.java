/**
 * Copyright Oct 22, 2012 Edward A. Webb
 * 
 */
package com.edwardawebb.atlassian.plugin.jira.msproject;

/**
 * @author Edward A. Webb
 */
public class ProjectAssignment {

	private String uid;
	private ProjectTask projectTask;
	private ProjectResource projectResource;
	
	public ProjectAssignment(ProjectTask projectTask,
			ProjectResource projectResource,String uid) {
		this.projectTask = projectTask;
		this.projectResource = projectResource;
		this.uid = uid;
	}
	
	public String getUid() {
		return uid;
	}

	public ProjectTask getProjectTask() {
		return projectTask;
	}
	
	public ProjectResource getProjectResource() {
		return projectResource;
	}
	
	
}
