/**
 * Copyright Oct 14, 2012 Edward A. Webb
 * 
 */
package com.edwardawebb.atlassian.plugin.jira.msproject;

import org.joda.time.Period;
import org.joda.time.Seconds;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;
import org.slf4j.LoggerFactory;

/**
 * A wrapper class fo Joda-Time's {@link Period} to provide a 
 * default toString method that is consumable as MS project format
 * @author Edward A. Webb
 */
public class ProjectDuration  {
	public static int DEFAULT_HOURS_PER_DAY = 8;
	
	private static org.slf4j.Logger LOG = LoggerFactory.getLogger(ProjectDuration.class);
	
	private Period period;

	private int workingHoursPerDay;
	
	/**
	 * Formats Joda periods into Project duration format, ie. - PT40H0M0S
	 */
	private static PeriodFormatter projectDurationFormatter = new PeriodFormatterBuilder()
		.appendLiteral("PT")
		.printZeroIfSupported()
		.appendHours()
		.appendSuffix("H")
		.appendMinutes()
		.appendSuffix("M")
		.appendMinutes()
		.appendSuffix("S")
		.toFormatter();
	

	
	public ProjectDuration(long longDate, int workingHoursPerDay) {
		this.period = new Period(longDate);
		this.workingHoursPerDay = workingHoursPerDay;
	}
	public ProjectDuration(Long longDate, int workingHoursPerDay) {
		this(longDate.longValue(),workingHoursPerDay);		
	}	
	public ProjectDuration(long longDate) {
		this.period = new Period(longDate);
		this.workingHoursPerDay = DEFAULT_HOURS_PER_DAY;
	}
	public ProjectDuration(Long longDate) {
		this(longDate.longValue(),DEFAULT_HOURS_PER_DAY);		
	}
	

	public void include(ProjectDuration additionalDuration) {
		this.period = this.period.plus(additionalDuration.period);
	}
	
	
	public Period getPeriod() {
		return period;
	}



	public String toString(){
		return period.toString(projectDurationFormatter);
	}
	public int getWorkingHoursPerDay() {
		return workingHoursPerDay;
	}
	public Seconds absoluteSeconds() {
		return this.period.toStandardSeconds();
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((period == null) ? 0 : period.hashCode());
		result = prime * result + workingHoursPerDay;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProjectDuration other = (ProjectDuration) obj;
		if (period == null) {
			if (other.period != null)
				return false;
		} else if (!period.equals(other.period))
			return false;
		if (workingHoursPerDay != other.workingHoursPerDay)
			return false;
		return true;
	}
	
	
	
}
