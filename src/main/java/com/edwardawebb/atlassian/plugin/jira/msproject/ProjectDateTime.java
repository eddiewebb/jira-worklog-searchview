/**
 * Copyright Oct 14, 2012 Edward A. Webb
 * 
 */
package com.edwardawebb.atlassian.plugin.jira.msproject;

import java.sql.Timestamp;
import java.util.Date;

import org.jfree.util.Log;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.LoggerFactory;

/**
 * Wrapper class for {@link DateTime} to expose custom toString that meets MS
 * project format
 * 
 * @author Edward A. Webb
 */
public class ProjectDateTime {
	private static org.slf4j.Logger LOG = LoggerFactory
			.getLogger(ProjectDateTime.class);
	public static int END_OF_DAY_HOUR = 17;
	public static int END_OF_DAY_MINUTE = 00;
	
	private DateTime dateTime;

	/**
	 * Formats Joda periods into Project DateTime format, ie. -
	 * 2012-08-01T08:00:00
	 */
	private static DateTimeFormatter projectDateTimeFormatter = DateTimeFormat
			.forPattern("yyyy-MM-dd'T'HH:mm:ss");

	public ProjectDateTime(Timestamp timestamp) {
		this.dateTime = new DateTime(timestamp);
	}

	public ProjectDateTime(DateTime dateTime) {
		this.dateTime = dateTime;
	}

	public ProjectDateTime(Date date) {
		this.dateTime = new DateTime(date);
	}
	
	/**
	 * Wrapper call to {@link ProjectDateTime}, if passed boolean is true the time of day will 
	 * be set to {@link ProjectDateTime.END_OF_DAY_HOUR} {@link ProjectDateTime.END_OF_DAY_MINUTE}
	 */
	public ProjectDateTime(Timestamp timestamp,boolean endOfDay) {
		this.dateTime = new DateTime(timestamp);
		if(endOfDay){
			this.dateTime = new DateTime(dateTime.getYear(),dateTime.getMonthOfYear(),dateTime.getDayOfMonth(),END_OF_DAY_HOUR,END_OF_DAY_MINUTE);
		}
	}

	/**
	 * Wrapper call to {@link ProjectDateTime}, if passed boolean is true the time of day will 
	 * be set to {@link ProjectDateTime.END_OF_DAY_HOUR} {@link ProjectDateTime.END_OF_DAY_MINUTE}
	 */
	public ProjectDateTime(DateTime dateTime,boolean endOfDay) {
		this(dateTime);
		if(endOfDay){
			this.dateTime = new DateTime(dateTime.getYear(),dateTime.getMonthOfYear(),dateTime.getDayOfMonth(),END_OF_DAY_HOUR,END_OF_DAY_MINUTE);
		}
	}

	/**
	 * Wrapper call to {@link ProjectDateTime}, if passed boolean is true the time of day will 
	 * be set to {@link ProjectDateTime.END_OF_DAY_HOUR} {@link ProjectDateTime.END_OF_DAY_MINUTE}
	 */
	public ProjectDateTime(Date date,boolean endOfDay) {
		this.dateTime = new DateTime(date);
		if(endOfDay){
			this.dateTime = new DateTime(dateTime.getYear(),dateTime.getMonthOfYear(),dateTime.getDayOfMonth(),END_OF_DAY_HOUR,END_OF_DAY_MINUTE);
		}
	}

	/**
	 * return underlying {@link DateTime} to do math or conversions
	 * 
	 * @return
	 */
	public DateTime getDateTime() {
		return dateTime;
	}

	public String toString() {
		return dateTime.toString(projectDateTimeFormatter);
	}

	/**
     * Assuming all due dates are end of day, this returns the start of corresponding effort
     * @param effort
     * @return
     */
    public ProjectDateTime toStartBasedOnEffort(ProjectDuration effort) {
            int hours = effort.getPeriod().toStandardHours().getHours();
            
            if(hours <= effort.getWorkingHoursPerDay()){
                    return new ProjectDateTime(dateTime.minus(effort.getPeriod()));
            }else{
                    ProjectDateTime startDate = new ProjectDateTime(dateTime);                     
    
                    do {
                            startDate.subtractDays(1);
                            if (startDate.getDateTime().getDayOfWeek()!= DateTimeConstants.SATURDAY
                                            && startDate.getDateTime().getDayOfWeek()!= DateTimeConstants.SUNDAY) {
                                    hours = hours - effort.getWorkingHoursPerDay();
                            }
                    } while (hours > effort.getWorkingHoursPerDay());
                    startDate.subtractHours(hours);
                    return  startDate;
            }
    }

	private void subtractHours(int remainder) {
		this.dateTime = this.dateTime.minusHours(remainder);		
	}

	private void subtractDays(int days) {
		this.dateTime = this.dateTime.minusDays(days);
		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((dateTime == null) ? 0 : dateTime.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProjectDateTime other = (ProjectDateTime) obj;
		if (dateTime == null) {
			if (other.dateTime != null)
				return false;
		} else if (!dateTime.equals(other.dateTime))
			return false;
		return true;
	}

	public boolean isBefore(ProjectDateTime start) {
		return this.dateTime.isBefore(start.dateTime);
	}

	public boolean isAfter(ProjectDateTime finish) {
		return this.dateTime.isAfter(finish.dateTime);
	}

	public boolean isBefore(Date date) {
		return this.dateTime.isBefore(date.getTime());
	}

	public boolean isAfter(Date date) {
		return this.dateTime.isAfter(date.getTime());
	}
	
	

}
