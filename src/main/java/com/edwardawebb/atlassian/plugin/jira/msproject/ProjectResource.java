/**
 * Copyright Oct 14, 2012 Edward A. Webb
 * 
 */
package com.edwardawebb.atlassian.plugin.jira.msproject;

import com.atlassian.jira.issue.Issue;

/**
 * Represents MS Project resource in XML
 * 
 * <Resource> <UID>1</UID> <ID>1</ID> <Name>Eddie</Name> <Type>1</Type>
 * <IsNull>0</IsNull> <Initials>E</Initials> <WorkGroup>0</WorkGroup>
 * <MaxUnits>1.00</MaxUnits> <PeakUnits>1.00</PeakUnits>
 * <OverAllocated>0</OverAllocated> <CanLevel>1</CanLevel>
 * <AccrueAt>3</AccrueAt> <Work>PT72H0M0S</Work>
 * <RegularWork>PT72H0M0S</RegularWork> <OvertimeWork>PT0H0M0S</OvertimeWork>
 * <ActualWork>PT0H0M0S</ActualWork> <RemainingWork>PT72H0M0S</RemainingWork>
 * <ActualOvertimeWork>PT0H0M0S</ActualOvertimeWork>
 * <RemainingOvertimeWork>PT0H0M0S</RemainingOvertimeWork>
 * <PercentWorkComplete>0</PercentWorkComplete> <StandardRate>0</StandardRate>
 * <StandardRateFormat>2</StandardRateFormat> <Cost>0</Cost>
 * <OvertimeRate>0</OvertimeRate> <OvertimeRateFormat>2</OvertimeRateFormat>
 * <OvertimeCost>0</OvertimeCost> <CostPerUse>0</CostPerUse>
 * <ActualCost>0</ActualCost> <ActualOvertimeCost>0</ActualOvertimeCost>
 * <RemainingCost>0</RemainingCost>
 * <RemainingOvertimeCost>0</RemainingOvertimeCost>
 * <WorkVariance>4320000.00</WorkVariance> <CostVariance>0</CostVariance>
 * <SV>0.00</SV> <CV>0.00</CV> <ACWP>0.00</ACWP> <CalendarUID>3</CalendarUID>
 * <BCWS>0.00</BCWS> <BCWP>0.00</BCWP> <IsGeneric>0</IsGeneric>
 * <IsInactive>0</IsInactive> <IsEnterprise>0</IsEnterprise>
 * <BookingType>0</BookingType> <CreationDate>2012-10-14T12:52:00</CreationDate>
 * <IsCostResource>0</IsCostResource> <IsBudget>0</IsBudget> </Resource>
 * 
 * @author Edward A. Webb
 */
public class ProjectResource {
	private String uid;
	private String name;
	private String initials;
	private ProjectDuration work = new ProjectDuration(0); //estimate
	private ProjectDuration remainingWork= new ProjectDuration(0); //remainint
	private ProjectDuration actualWork= new ProjectDuration(0);//recorded
	private String id;

	
	public ProjectResource(String uid) {
		this.uid = uid;
	}
	
	public void ammendWork(ProjectTask task) {		
		this.work.include(task.getOriginalEffort());
		this.actualWork.include(task.getActualWork());
		this.remainingWork.include(task.getRemainingWork());
	}
	
	public String getUid() {
		return uid;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInitials() {
		return initials;
	}

	public void setInitials(String initials) {
		this.initials = initials;
	}

	public ProjectDuration getWork() {
		return work;
	}

	public void setWork(ProjectDuration work) {
		this.work = work;
	}

	public ProjectDuration getRemainingWork() {
		return remainingWork;
	}

	public void setRemainingWork(ProjectDuration remainingWork) {
		this.remainingWork = remainingWork;
	}

	public ProjectDuration getActualWork() {
		return actualWork;
	}

	public void setActualWork(ProjectDuration actualWork) {
		this.actualWork = actualWork;
	}





}
