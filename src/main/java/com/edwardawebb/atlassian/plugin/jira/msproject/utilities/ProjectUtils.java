/**
 * Copyright Oct 14, 2012 Edward A. Webb
 * 
 */
package com.edwardawebb.atlassian.plugin.jira.msproject.utilities;

import java.util.StringTokenizer;

/**
 * @author Edward A. Webb
 */
public class ProjectUtils {
	
	
 
	//TODO - add logic for short names (ie display is only one word)
	public static String buildInitials(String displayName) {
		StringBuilder initials = new StringBuilder();
		StringTokenizer tokenizer = new StringTokenizer(displayName);
		while (tokenizer.hasMoreTokens()) {
			initials.append(tokenizer.nextToken().charAt(0));
		}
		return initials.toString();
	}


}
